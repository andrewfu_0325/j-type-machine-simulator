# README #

### How do I get set up? ###
	1. Use "make" or "make debug" to compile this project.
	2. All parameters are set in the "main.cc". Re-compilation is required after parameter tweaking.
	3. Use "make run" or "make run-debug" correspondingly to run the simulation.
	4. The timestamps is dumped to a .csv file named "timestamp.csv" when simulation is complete.
	5. Use "make clean" to clean out all object files, executable files and the timestamp.csv.
