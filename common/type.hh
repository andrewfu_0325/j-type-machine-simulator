#ifndef _TYPE_HH_
#define _TYPE_HH_

#include <string>

#define Tick signed int

struct TimeStamp {
	std::string name;
	Tick place_time;
	Tick process_begin_time;
	Tick process_end_time;
	Tick pick_time;
};

#endif

