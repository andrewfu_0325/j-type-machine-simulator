
#ifndef _DEVICE_HH_
#define _DEVICE_HH_

#include "type.hh"
#include <string>


class Device {
	public:
		Device(std::string _name) : name(_name) {}
		virtual ~Device() {}
		virtual void updateState(Tick elapsed_time) = 0;
	protected:
		std::string name;
};

#endif

