#ifndef _DEBUG_HH_
#define _DEBUG_HH_

#include <cstdarg>

#ifdef DEBUG
#define DEBUG_PRINT(...) fprintf(stderr,  __VA_ARGS__);
#else
#define DEBUG_PRINT(...) do{} while(false)
#endif

#endif

