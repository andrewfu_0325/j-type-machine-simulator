
#ifndef _JOB_HH_
#define _JOB_HH_

#include "type.hh"

class Job {
  public:
    explicit Job(Tick _start) :
      start(_start), end(0) {}
    virtual ~Job() {}
    virtual void process() = 0;
  protected:
    Tick start;
    Tick end;
};

#endif

