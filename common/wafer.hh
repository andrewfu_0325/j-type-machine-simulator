
#ifndef _WAFER_HH_
#define _WAFER_HH_

#include "type.hh"

class Wafer {
	public:
		Wafer(unsigned _id) :
			id(_id), wet_wafer_wait_start(0), wet_wafer_wait_end(0),
			complete_time(0) {}
		std::string timeStampToString() {
			std::string str;
			str += "CAS" + std::to_string(id) + ',';
			for(auto &ts_it : timestamps) {
				str += ts_it.name + ',' +
					std::to_string(ts_it.place_time) + ','+
					std::to_string(ts_it.process_begin_time)+ ','+
					std::to_string(ts_it.process_end_time)+ ','+
					std::to_string(ts_it.pick_time) + ',';
			}
			return str;
		}
		unsigned getID() {return id;}
		Tick getCompleteTime() {return complete_time;}
		Tick getWetWaferWaitStart() {return wet_wafer_wait_start;}
		Tick getWetWaferWaitEnd() {return wet_wafer_wait_end;}
		void addTimeStampForModule() {timestamps.push_back(TimeStamp());}
		void setCompleteTime(Tick _complete_time) {complete_time = _complete_time;}
		void setPlaceTime(std::string name, Tick time) {
			timestamps.back().name = name; 
			timestamps.back().place_time = time;
		}
		void setProcessBeginTime(Tick time) {
			timestamps.back().process_begin_time = time;
		}
		void setProcessEndTime(Tick time) {
			timestamps.back().process_end_time = time;
		}
		void setPickTime(Tick time) {
			timestamps.back().pick_time = time;
		}
		void setWetWaferWaitStart(Tick start) {wet_wafer_wait_start = start;}
		void setWetWaferWaitEnd(Tick end) {wet_wafer_wait_end = end;}
	private:
		unsigned id;
		Tick wet_wafer_wait_start;
		Tick wet_wafer_wait_end;
		Tick complete_time;
		std::vector<TimeStamp> timestamps;
};

#endif

