
#ifndef _EVENT_HH_
#define _EVENT_HH_

#include "device.hh"
#include "type.hh"

#include <cassert>
#include <cstdio>
#include <queue>

class Event {
	public:
		Event(Tick _start_time) : start_time(_start_time) {}
		virtual ~Event() {}
		virtual void process() = 0;
		Tick getStartTime() const {return start_time;}
	protected:
		Tick start_time;
};

class EventComparator {
	public:
		bool operator() (const Event *lhs, const Event *rhs) const {
			return (lhs->getStartTime() > rhs->getStartTime());
		}
};

class EventScheduler {
	public:
		EventScheduler() : current_time{0} {}
		bool isAdvance() {return (event_queue.size() == 0 || 
				event_queue.top()->getStartTime() != current_time);}
		Tick getElapsedTime() {
			assert(event_queue.size() != 0);
			return (event_queue.top()->getStartTime() - current_time);
		}


		void addDevice(Device *dev) {
			devices.push_back(dev);
		}
		void processEvent() {
			assert(event_queue.size() > 0);
			Event *event = event_queue.top();
			event_queue.pop();
			assert(event != nullptr);
			event->process();
			delete event;
		}

		void updateState(Tick elapsed_time) {
			current_time += elapsed_time;
			for(auto it : devices) {
				it->updateState(elapsed_time);
			}
		}

		void schedule(Event *event) {event_queue.push(event);}

		Tick getCurrentTime() const {return current_time;}
	private:
		std::priority_queue<Event*, std::vector<Event*>, EventComparator> event_queue;
		std::vector<Device*> devices;
		Tick current_time;
};

#endif

