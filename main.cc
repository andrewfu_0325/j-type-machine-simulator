
#include "common/event.hh"
#include "dev/buffer.hh"
#include "dev/center.hh"
#include "dev/robot_tcm.hh"
#include "dev/robot_soak.hh"
#include "dev/loader.hh"
#include "dev/soaker.hh"
#include "dev/tcm.hh"

#include <cassert>

#define NUM_WAFERS 50
#define MAX_SOAKERS 6
#define NUM_SOAKERS 2
#define NUM_SOAKING_WAFERS 6
#define SOAKER_PROCESS_TIME 300
#define SOAKER_RISE_TIME 20
#define SOAKER_SINK_TIME 20
#define SOAKER_INTERVAL 50

#define MAX_TCMS 6
#define NUM_TCMS 2
#define TCM_PROCESS_TIME 100

#define CENTER_PROCESS_TIME 6
#define BUFFER_PROCESS_TIME 8

#define WET_WAFER_TOLERENCE_TIME 80
#define RESERVED_TIME_FOR_TCM 60

#define ROBOT_SOAK_ROTATE_TIME 2
#define ROBOT_SOAK_FETCH_C_TIME 6
#define ROBOT_SOAK_FETCH_S_TIME 6
#define ROBOT_SOAK_PLACE_B_TIME 6
#define ROBOT_SOAK_PLACE_S_TIME 6

#define ROBOT_TCM_ROTATE_TIME 2
#define ROBOT_TCM_FETCH_B_TIME 6
#define ROBOT_TCM_FETCH_L_TIME 6
#define ROBOT_TCM_FETCH_T_TIME 6
#define ROBOT_TCM_PLACE_C_TIME 6
#define ROBOT_TCM_PLACE_L_TIME 6
#define ROBOT_TCM_PLACE_T_TIME 6

std::string arg_to_string() {
	std::string arg_str;
	arg_str +=
		"NUM_WAFERS," + std::to_string(NUM_WAFERS) + ',' +
		"NUM_SOAKERS," + std::to_string(NUM_SOAKERS) + ',' +
		"NUM_TCMS," +	std::to_string(NUM_TCMS) + ',' +
		"RESERVED_TIME_FOR_TCM," + std::to_string(RESERVED_TIME_FOR_TCM) + ',' +
		"ROBOT_SOAK_ROTATE_TIME," + std::to_string(ROBOT_SOAK_ROTATE_TIME) + ',' +
		"ROBOT_TCM_ROTATE_TIME," + std::to_string(ROBOT_TCM_ROTATE_TIME) + '\n' +

		",,NUM_SOAKING_WAFERS," +	std::to_string(NUM_SOAKING_WAFERS) + ',' +
		"TCM_PROCESS_TIME," +	std::to_string(TCM_PROCESS_TIME) + ',' +
		"WET_WAFER_TOLERENCE_TIME," + std::to_string(WET_WAFER_TOLERENCE_TIME) + ',' +
		"ROBOT_SOAK_FETCH_C_TIME," + std::to_string(ROBOT_SOAK_FETCH_C_TIME) + ',' +
		"ROBOT_TCM_FETCH_L_TIME," +	std::to_string(ROBOT_TCM_FETCH_L_TIME) + '\n' +

		",,SOAKER_PROCESS_TIME," + std::to_string(SOAKER_PROCESS_TIME) + ',' +
		"CENTER_PROCESS_TIME," + std::to_string(CENTER_PROCESS_TIME) + ',' +
		",,ROBOT_SOAK_PLACE_S_TIME," + std::to_string(ROBOT_SOAK_PLACE_S_TIME) + ',' +
		"ROBOT_TCM_PLACE_C_TIME," + std::to_string(ROBOT_TCM_PLACE_C_TIME) + '\n' +

		",,SOAKER_RISE_TIME," +	std::to_string(SOAKER_RISE_TIME) + ',' +
		"BUFFER_PROCESS_TIME," + std::to_string(BUFFER_PROCESS_TIME) + ',' +
		",,ROBOT_SOAK_FETCH_S_TIME," + std::to_string(ROBOT_SOAK_FETCH_S_TIME) + ',' +
		"ROBOT_TCM_FETCH_B_TIME," + std::to_string(ROBOT_TCM_FETCH_B_TIME) + '\n' +

		",,SOAKER_SINK_TIME," +	std::to_string(SOAKER_SINK_TIME) + ',' +
		",,,,ROBOT_SOAK_PLACE_B_TIME," + std::to_string(ROBOT_SOAK_PLACE_B_TIME) + ',' +
		"ROBOT_TCM_PLACE_T_TIME," + std::to_string(ROBOT_TCM_PLACE_T_TIME) + '\n' +

		",,SOAKER_INTERVAL," + std::to_string(SOAKER_INTERVAL) + ',' +
		",,,,,,ROBOT_TCM_FETCH_T_TIME," + std::to_string(ROBOT_TCM_FETCH_T_TIME) + '\n' +

		",,,,,,,,,,ROBOT_TCM_PLACE_L_TIME," + std::to_string(ROBOT_TCM_PLACE_L_TIME) + '\n';
	return arg_str;
}

int main(int argc, char **argv) {
	assert(SOAKER_INTERVAL <= SOAKER_PROCESS_TIME + SOAKER_SINK_TIME);
	assert(NUM_SOAKERS <= MAX_SOAKERS);
	assert(NUM_TCMS <= MAX_TCMS);
	EventScheduler event_scheduler;

	Loader loader(&event_scheduler, "LULA", NUM_WAFERS, WET_WAFER_TOLERENCE_TIME);
	event_scheduler.addDevice(static_cast<Device*>(&loader));

	Center center(&event_scheduler, "HOBIn", CENTER_PROCESS_TIME);
	event_scheduler.addDevice(static_cast<Device*>(&center));

	Buffer buffer(&event_scheduler, "HOBOut", BUFFER_PROCESS_TIME);
	event_scheduler.addDevice(static_cast<Device*>(&buffer));

	RobotSoak robot_soak(&event_scheduler,
			"RobotSoak",
			&center, &buffer,
			ROBOT_SOAK_ROTATE_TIME,
			ROBOT_SOAK_FETCH_C_TIME,
			ROBOT_SOAK_FETCH_S_TIME,
			ROBOT_SOAK_PLACE_B_TIME,
			ROBOT_SOAK_PLACE_S_TIME,
			RESERVED_TIME_FOR_TCM);
	event_scheduler.addDevice(static_cast<Device*>(&robot_soak));

	RobotTCM robot_tcm(&event_scheduler,
			"RobotTCM",
			&loader, &center, 
			&buffer,
			ROBOT_TCM_ROTATE_TIME,
			ROBOT_TCM_FETCH_L_TIME,
			ROBOT_TCM_FETCH_B_TIME,
			ROBOT_TCM_FETCH_T_TIME,
			ROBOT_TCM_PLACE_C_TIME,
			ROBOT_TCM_PLACE_L_TIME,
			ROBOT_TCM_PLACE_T_TIME);
	event_scheduler.addDevice(static_cast<Device*>(&robot_tcm));

	for(int i = 1; i < NUM_SOAKERS + 1; i++) {
		Soaker *soaker = new Soaker(&event_scheduler,
				std::string("SoakingTank") + char('A' + i - 1),
				NUM_SOAKING_WAFERS,
				i,
				SOAKER_PROCESS_TIME,
				SOAKER_RISE_TIME,
				SOAKER_SINK_TIME,
				SOAKER_INTERVAL);
		robot_soak.addSoaker(soaker);
		event_scheduler.addDevice(static_cast<Device*>(soaker));
	}

	for(int i = 1; i < NUM_TCMS + 1; i++) {
		TCM *tcm = new TCM(&event_scheduler,
				std::string("Stripper") + char('A' + i - 1),
				TCM_PROCESS_TIME, 
				RESERVED_TIME_FOR_TCM,
				i);
		robot_tcm.addTCM(tcm);
		event_scheduler.addDevice(static_cast<Device*>(tcm));
	}

	robot_tcm.setRobotSoak(&robot_soak);
	robot_soak.setRobotTCM(&robot_tcm);
	robot_soak.initDev();
	robot_tcm.initDev();

	for(int i = 1; i < NUM_WAFERS + 1; i++) {
		Wafer *wafer = new Wafer(i);
		loader.addWafer(wafer);
	}

	while(!loader.complete()) {
		if(event_scheduler.isAdvance()) {
			robot_soak.initJob();
			robot_tcm.initJob();
			Tick elapsed_time = event_scheduler.getElapsedTime();
			assert(elapsed_time != 0);
			event_scheduler.updateState(elapsed_time);
		}
		event_scheduler.processEvent();
	}

	loader.dumpResults(arg_to_string());
	robot_soak.clearDev();
	robot_tcm.clearDev();

}

