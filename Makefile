
CC=g++

SRCDIR = dev
OBJDIR = obj
SRC := $(wildcard $(SRCDIR)/*.cc)
OBJ := $(SRC:$(SRCDIR)/%.cc=$(OBJDIR)/%.o)

CCFLAG=-std=c++11 -Wall

all : simulator

simulator : main.cc $(OBJ)
	$(CC) $(CCFLAG) $^ -o $@

simulator-debug : main.cc $(OBJ)
	$(CC) $(CCFLAG) $^ -o $@

$(OBJ): $(OBJDIR)/%.o : $(SRCDIR)/%.cc
	@if [ ! -d $(OBJDIR) ]; then mkdir $(OBJDIR); fi
	$(CC) $(CCFLAG) -c $^ -o $@

debug : CCFLAG+=-DDEBUG -g
debug : simulator-debug


run:
	./simulator

run-debug:
	./simulator-debug

gdb:
	gdb simulator-debug

clean:
	rm -rf $(OBJDIR) simulator simulator-debug timestamp.csv

