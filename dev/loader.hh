
#ifndef _LOADER_HH_
#define _LOADER_HH_

#include "../common/event.hh"
#include "../common/debug.hh"
#include "../common/device.hh"
#include "../common/wafer.hh"

#include <cstdio>
#include <string>
#include <queue>

class Loader : public Device {
	public:
		explicit Loader(EventScheduler *_event_scheduler,
				std::string _name, unsigned _num_wafers, 
				Tick _wet_wafer_tolerence_time) : Device(_name),
			event_scheduler(_event_scheduler), num_wafers(_num_wafers),
			wet_wafer_tolerence_time(_wet_wafer_tolerence_time) {}

		bool empty() const {return (pending_wafers.size() == 0);}
		bool complete() const {return (complete_wafers.size() == num_wafers);}
		Wafer *getAndRemoveWafer();
		Wafer *getFirstWafer() const {return pending_wafers.front();}

		void addWafer(Wafer *wafer);
		void addCompleteWafer(Wafer *wafer);
		void dumpResults(std::string arg_str);
		void placeWafer(Wafer *wafer) {complete_wafers.push(wafer);}
		void updateState(Tick elapsed_time) {};
	private:
		EventScheduler *event_scheduler;
		std::queue<Wafer*> pending_wafers;
		std::queue<Wafer*> complete_wafers;
		unsigned num_wafers;
		Tick wet_wafer_tolerence_time;
};

#endif

