
#ifndef _CENTER_HH_
#define _CENTER_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/type.hh"
#include "../common/wafer.hh"

#include <cassert>
#include <string>

class RobotSoak;
class RobotTCM;
class RobotTCMJob;

class Center : public Device{
	public:
		explicit Center(EventScheduler *_event_scheduler,
			std::string _name,
			Tick _process_time) : Device(_name),
			event_scheduler(_event_scheduler), current_job(nullptr),
			pending_robot_tcm_job(nullptr), robot_soak(nullptr), 
			robot_tcm(nullptr), wafer(nullptr),
			process_time(_process_time), rest_time(0) {}
	private:
		class CenterJob : public Job {
			public:
				CenterJob(Tick _start_time, Center *_center) :
					Job(_start_time), clear_job(false), center(_center) {}
				bool isClearJob() {return clear_job;}
				Center *getCenter() {return center;}
			protected:
				bool clear_job;
				Center *center;
		};

		class ProcessJob : public CenterJob {
			public:
				ProcessJob(Tick _start_time, Center *_center) :
					CenterJob(_start_time, _center), state(PROCESS) {}
				void virtual process();
			private:
				enum State {
					PROCESS,
					COMPLETE
				};
				State state;
		};

		class CenterEvent : public Event {
			public:
				explicit CenterEvent(Tick _start_time, CenterJob *_job) :
					Event(_start_time), job(_job) {}
				void process();
			private:
				CenterJob *job;
		};

	public:
		bool complete () const {return rest_time == 0;}
		bool full() const {return (wafer != nullptr);}
		CenterJob *getCurrentJob() const {return current_job;}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		RobotSoak *getRobotSoak() const {return robot_soak;}
		RobotTCM *getRobotTCM() const {return robot_tcm;}
		Tick getProcessTime() const {return process_time;}
		Tick getRestTime() const {return rest_time;}
		Wafer *getAndRemoveWafer();
		Wafer *getWafer() const {return wafer;}

		void addWafer(Wafer *_wafer);
		void clearJob();
		void initRobotTCMJob();
		void initRobotSoakJob();
		void setPendingRobotTCMJob(RobotTCMJob* job) {
			assert(pending_robot_tcm_job == nullptr);
			pending_robot_tcm_job = job;
		}
		void setRestTime() {rest_time = process_time;}
		void setRobotSoak(RobotSoak *_robot_soak) {robot_soak = _robot_soak;}
		void setRobotTCM(RobotTCM *_robot_tcm) {robot_tcm = _robot_tcm;}
		void updateState(Tick elapsed_time);

	private:
		EventScheduler *event_scheduler;
		CenterJob *current_job;
		RobotTCMJob* pending_robot_tcm_job;
		RobotSoak *robot_soak;
		RobotTCM *robot_tcm;

		Wafer *wafer;
		Tick process_time;
		Tick rest_time;
};

#endif

