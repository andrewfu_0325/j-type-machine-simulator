
#include "../common/debug.hh"
#include "robot_soak.hh"
#include "robot_tcm.hh"

#include <typeinfo>

void RobotTCM::clearDev() {
	for(auto tcm_it : tcms) {
		delete tcm_it;
	}
}

void RobotTCM::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void RobotTCM::initDev() {
	buffer->setRobotTCM(this);
	center->setRobotTCM(this);
	for(auto tcm_it : tcms) {
		tcm_it->setRobotTCM(this);
	}
}

void RobotTCM::initJob() {
	Tick current_tick = event_scheduler->getCurrentTime();
	if(current_job != nullptr) {
		return;
	}

	TCM *tcm;
	// B2T Job
	bool isB2TJob = false;
	if(buffer->full() &&
			buffer->getRestTime() <= ((pos==B)?0:rotate_time)) {
		for(auto tcm_it : tcms) {
			if(!tcm_it->full()) {
				isB2TJob = true;
				tcm = tcm_it;
				break;
			}
		}
	}
	if(isB2TJob) {
		DEBUG_PRINT("Initialize B2TJob(%u) of RobotTCM @ Tick %u\n",
				tcm->getID(),
				current_tick);
		current_job = new B2TJob(current_tick, this, tcm, static_cast<Position>(tcm->getID()));
		current_job->process();
		return;
	}

	// B2T2L Job
	bool isB2T2LJob = false;
	if(buffer->full() && 
			buffer->getRestTime() <= ((pos==B)?0:rotate_time)) {
		for(auto tcm_it : tcms) {
			if(tcm_it->full() && 
					tcm_it->getRestTime() <= 
					rotate_time + ((pos==B)?0:rotate_time) + fetch_b_time) {
				isB2T2LJob = true;
				tcm = tcm_it;
				break;
			}
		}
	}
	if(isB2T2LJob) {
		DEBUG_PRINT("Initialize B2T2LJob(%u) of RobotTCM @ Tick %u\n",
				tcm->getID(),
				current_tick);
		current_job = new B2T2LJob(current_tick, this, tcm, static_cast<Position>(tcm->getID()));
		current_job->process();
		return;
	}

	// L2C Job
	bool deadlock = center->full() && buffer->full();
	if(robot_soak->busy()) {
		deadlock &=
			(typeid(*robot_soak->getCurrentJob()) == typeid(RobotSoak::C2S2BJob) ||
			 typeid(*robot_soak->getCurrentJob()) == typeid(RobotSoak::S2BJob));
	} else {
		deadlock &= robot_soak->isCPrefetched();
	}
	if(!getLoader()->empty() &&
			center->getRestTime() <= 
			rotate_time + ((pos==L)?0:rotate_time) + fetch_l_time &&
			/* To preven deadlock between RobotTCM and RobotSoak */
			!deadlock) {
		DEBUG_PRINT("Initialize L2CJob of RobotTCM @ Tick %u\n",
				current_tick);
		current_job = new L2CJob(current_tick, this);
		current_job->process();
		return;
	}

	// T2L Job
	bool isT2LJob = false;
	if(!buffer->full()) {
		for(auto tcm_it : tcms) {
			if(tcm_it->full() &&
					tcm_it->getRestTime() <= ((pos==tcm_it->getID())?0:rotate_time)) {
				isT2LJob = true;
				tcm = tcm_it;
				break;
			}
		}
	}
	if(isT2LJob) {
		DEBUG_PRINT("Initialize T2LJob(%u) of RobotTCM @ Tick %u\n",
				tcm->getID(),
				current_tick);
		current_job = new T2LJob(current_tick, this, tcm, static_cast<Position>(tcm->getID()));
		current_job->process();
		return;
	}
}

void RobotTCM::fetchBuffer() {
	assert(wet_wafer == nullptr);
	wet_wafer = buffer->getAndRemoveWafer();
}

void RobotTCM::fetchLoader() {
	assert(dry_wafer == nullptr);
	dry_wafer = loader->getAndRemoveWafer();
}

void RobotTCM::fetchTCM(TCM *tcm) {
	assert(dry_wafer == nullptr);
	dry_wafer = tcm->getAndRemoveWafer();
}

void RobotTCM::placeCenter() {
	assert(dry_wafer != nullptr); 
	center->addWafer(dry_wafer);
	dry_wafer = nullptr;
}

void RobotTCM::placeTCM(TCM *tcm) {
	assert(wet_wafer != nullptr);
	tcm->addWafer(wet_wafer);
	tcm->resetReserved();
	wet_wafer = nullptr;
}

void RobotTCM::placeLoader() {
	assert(dry_wafer !=nullptr);
	loader->addCompleteWafer(dry_wafer);
	dry_wafer->setCompleteTime(event_scheduler->getCurrentTime());
	dry_wafer = nullptr;
}

void RobotTCM::B2T2LJob::process() {
	assert(robot_tcm->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_tcm->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotTCMEvent *event;
	switch(state) {
		case ROTATE_TO_B:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: ROTATE_TO_B @ Tick %u\n",
					tcm_pos,
					current_tick);
			state = FETCH_B;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_B:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: FETCH_B, WET_WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getBuffer()->getWafer()->getID(),
					current_tick);
			robot_tcm->setPos(B);
			state = ROTATE_TO_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getFetchBTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_T:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: ROTATE_TO_T @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->fetchBuffer();
			state = FETCH_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_T:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: FETCH_T, DRY_WID: %u @ Tick %u\n",
					tcm_pos,
					tcm->getWafer()->getID(),
					current_tick);
			robot_tcm->setPos(tcm_pos);
			state = PLACE_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getFetchTTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_T:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: PLACE_T, WET_WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getWetWafer()->getID(),
					current_tick);
			robot_tcm->fetchTCM(tcm);
			robot_tcm->getWetWafer()->setWetWaferWaitEnd(current_tick);
			state = ROTATE_TO_L;
			event = new RobotTCMEvent(current_tick + robot_tcm->getPlaceTTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_L:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: ROTATE_TO_L @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->placeTCM(tcm);
			state = PLACE_L;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_L:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: PLACE_L, DRY_WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getDryWafer()->getID(),
					current_tick);
			robot_tcm->setPos(L);
			state = COMPLETE;
			event = new RobotTCMEvent(current_tick + robot_tcm->getPlaceLTime(), this);
			event_scheduler->schedule(event);
			break;
		case COMPLETE:
			DEBUG_PRINT("B2T2LJob(%u) of RobotTCM: COMPLETE @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->placeLoader();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for B2T2LJob of RobotTCM!\n", state);
			exit(EXIT_FAILURE);
	};
}

void RobotTCM::B2TJob::process() {
	assert(robot_tcm->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_tcm->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotTCMEvent *event;
	switch(state) {
		case ROTATE_TO_B:
			DEBUG_PRINT("B2TJob(%u) of RobotTCM: ROTATE_TO_B @ Tick %u\n",
					tcm_pos,
					current_tick);
			state = FETCH_B;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_B:
			DEBUG_PRINT("B2TJob(%u) of RobotTCM: FETCH_B, WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getBuffer()->getWafer()->getID(),
					current_tick);
			robot_tcm->setPos(B);
			state = ROTATE_TO_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getFetchBTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_T:
			DEBUG_PRINT("B2TJob(%u) of RobotTCM: ROTATE_TO_T @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->fetchBuffer();
			state = PLACE_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_T:
			DEBUG_PRINT("B2TJob(%u) of RobotTCM: PLACE_T, WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getWetWafer()->getID(),
					current_tick);
			robot_tcm->setPos(tcm_pos);
			robot_tcm->getWetWafer()->setWetWaferWaitEnd(current_tick);
			state = COMPLETE;
			event = new RobotTCMEvent(current_tick + robot_tcm->getPlaceTTime(), this);
			event_scheduler->schedule(event);
			break;
		case COMPLETE:
			DEBUG_PRINT("B2TJob(%u) of RobotTCM: COMPLETE @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->placeTCM(tcm);
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for B2TJob of RobotTCM!\n", state);
			exit(EXIT_FAILURE);
	};
}

void RobotTCM::T2LJob::process() {
	assert(robot_tcm->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_tcm->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotTCMEvent *event;
	switch(state) {
		case ROTATE_TO_T:
			DEBUG_PRINT("T2LJob(%u) of RobotTCM: ROTATE_TO_T @ Tick %u\n",
					tcm_pos,
					current_tick);
			state = FETCH_T;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_T:
			DEBUG_PRINT("T2LJob(%u) of RobotTCM: FETCH_T, WID: %u @ Tick %u\n",
					tcm_pos,
					tcm->getWafer()->getID(),
					current_tick);
			robot_tcm->setPos(tcm_pos);
			state = ROTATE_TO_L;
			event = new RobotTCMEvent(current_tick + robot_tcm->getFetchTTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_L:
			DEBUG_PRINT("T2LJob(%u) of RobotTCM: ROTATE_TO_L @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->fetchTCM(tcm);
			state = PLACE_L;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_L:
			DEBUG_PRINT("T2LJob(%u) of RobotTCM: PLACE_L, WID: %u @ Tick %u\n",
					tcm_pos,
					robot_tcm->getDryWafer()->getID(),
					current_tick);
			robot_tcm->setPos(L);
			state = COMPLETE;
			event = new RobotTCMEvent(current_tick + robot_tcm->getPlaceLTime(), this);
			event_scheduler->schedule(event);
			break;
		case COMPLETE:
			DEBUG_PRINT("T2LJob(%u) of RobotTCM: COMPLETE @ Tick %u\n",
					tcm_pos,
					current_tick);
			robot_tcm->placeLoader();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for T2LJob of RobotTCM!\n", state);
			exit(EXIT_FAILURE);
	};
}

void RobotTCM::L2CJob::process() {
	assert(robot_tcm->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_tcm->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotTCMEvent *event;
	Center *center;
	switch(state) {
		case ROTATE_TO_L:
			DEBUG_PRINT("L2CJob of RobotTCM: ROTATE_TO_L @ Tick %u\n",
					current_tick);
			state = FETCH_L;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_L:
			DEBUG_PRINT("L2CJob of RobotTCM: FETCH_L, WID: %u @ Tick %u\n",
					robot_tcm->getLoader()->getFirstWafer()->getID(),
					current_tick);
			robot_tcm->setPos(L);
			state = ROTATE_TO_C;
			event = new RobotTCMEvent(current_tick + robot_tcm->getFetchLTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_C:
			DEBUG_PRINT("L2CJob of RobotTCM: ROTATE_TO_C @ Tick %u\n",
					current_tick);
			robot_tcm->fetchLoader();
			state = PLACE_C;
			event = new RobotTCMEvent(current_tick + robot_tcm->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_C:
			robot_tcm->setPos(C);
			center = robot_tcm->getCenter();
			if (center->full()) {
				DEBUG_PRINT("L2CJob of RobotTCM: PLACE_C, WID: %u (wait for center empty), @ Tick %u\n", 
						robot_tcm->getDryWafer()->getID(), 
						current_tick);
				center->setPendingRobotTCMJob(this);
				state = PLACE_C;
			} else {
				DEBUG_PRINT("L2CJob of RobotTCM: PLACE_C, WID: %u, @ Tick %u\n", 
						robot_tcm->getDryWafer()->getID(), 
						current_tick);
				state = COMPLETE;
				event = new RobotTCMEvent(current_tick + robot_tcm->getPlaceCTime(), this);
				event_scheduler->schedule(event);
			}
			break;
		case COMPLETE:
			DEBUG_PRINT("L2CJob of RobotTCM: Completed @ Tick %u\n",
					current_tick);
			robot_tcm->placeCenter();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for L2CJob of RobotTCM!\n", state);
			exit(EXIT_FAILURE);
	}
}

void RobotTCM::RobotTCMEvent::process() {
	assert(job != nullptr);
	job->process();
	if(job->isClearJob()) {
		RobotTCM *robot_tcm = job->getRobotTCM();
		robot_tcm->clearJob();
	}
}

