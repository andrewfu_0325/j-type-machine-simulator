
#ifndef _BUFFER_HH_
#define _BUFFER_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/type.hh"
#include "../common/wafer.hh"

#include <cassert>
#include <string>

class RobotSoak;
class RobotSoakJob;
class RobotTCM;

class Buffer : public Device{
	public:
		explicit Buffer(EventScheduler *_event_scheduler,
			std::string _name,
			Tick _process_time) : Device(_name),
			event_scheduler(_event_scheduler), current_job(nullptr),
			pending_robot_soak_job(nullptr), robot_soak(nullptr), 
			robot_tcm(nullptr), wafer(nullptr),
			process_time(_process_time), rest_time(0) {}
	private:
		class BufferJob : public Job {
			public:
				BufferJob(Tick _start_time, Buffer *_buffer) :
					Job(_start_time), clear_job(false), buffer(_buffer) {}
				bool isClearJob() {return clear_job;}
				Buffer *getBuffer() {return buffer;}
			protected:
				bool clear_job;
				Buffer *buffer;
		};

		class ProcessJob : public BufferJob {
			public:
				ProcessJob(Tick _start_time, Buffer *_buffer) :
					BufferJob(_start_time, _buffer), state(PROCESS) {}
				void virtual process();
			private:
				enum State {
					PROCESS,
					COMPLETE
				};
				State state;
		};

		class BufferEvent : public Event {
			public:
				explicit BufferEvent(Tick _start_time, BufferJob *_job) :
					Event(_start_time), job(_job) {}
				void process();
			private:
				BufferJob *job;
		};

	public:
		bool complete () const {return rest_time == 0;}
		bool full() const {return (wafer != nullptr);}
		BufferJob *getCurrentJob() const {return current_job;}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		RobotSoak *getRobotSoak() const {return robot_soak;}
		RobotTCM *getRobotTCM() const {return robot_tcm;}
		Tick getProcessTime() const {return process_time;}
		Tick getRestTime() const {return rest_time;}
		Wafer *getAndRemoveWafer();
		Wafer *getWafer() const {return wafer;}

		void addWafer(Wafer *_wafer);
		void clearJob();
		void initRobotTCMJob();
		void setPendingRobotSoakJob(RobotSoakJob* job) {
			assert(pending_robot_soak_job == nullptr);
			pending_robot_soak_job = job;
		}
		void setRobotSoak(RobotSoak *_robot_soak) {robot_soak = _robot_soak;}
		void setRobotTCM(RobotTCM *_robot_tcm) {robot_tcm = _robot_tcm;}
		void updateState(Tick elapsed_time);

	private:
		EventScheduler *event_scheduler;
		BufferJob *current_job;
		RobotSoakJob* pending_robot_soak_job;
		RobotSoak *robot_soak;
		RobotTCM *robot_tcm;

		Wafer *wafer;
		Tick process_time;
		Tick rest_time;
};

#endif

