
#ifndef _ROBOT_TCM_HH_
#define _ROBOT_TCM_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/wafer.hh"
#include "buffer.hh"
#include "center.hh"
#include "loader.hh"
#include "tcm.hh"

#include <string>
#include <vector>

class RobotSoak;

class RobotTCMJob : public Job {
	public:
		RobotTCMJob(Tick _start_time, RobotTCM *_robot_tcm) :
			Job(_start_time), clear_job(false), robot_tcm(_robot_tcm) {}
		bool isClearJob() {return clear_job;}
		RobotTCM *getRobotTCM() {return robot_tcm;}
	protected:
		bool clear_job;
		RobotTCM *robot_tcm;
};

class RobotTCM : public Device {
	public:
		explicit RobotTCM(EventScheduler *_event_scheduler,
				std::string _name,
				Loader *_loader, Center *_center,
				Buffer *_buffer, Tick _rotate_time, 
				Tick _fetch_b_time, Tick _fetch_l_time, 
				Tick _place_c_time, Tick _fetch_t_time,
				Tick _place_l_time, Tick _place_t_time) : Device(_name),
			event_scheduler(_event_scheduler), 
			current_job(nullptr),
			loader(_loader), center(_center), 
			buffer(_buffer), 
			dry_wafer(nullptr), wet_wafer(nullptr), pos(L),
			rotate_time(_rotate_time), fetch_b_time(_fetch_b_time),
			fetch_l_time(_fetch_l_time), fetch_t_time(_fetch_t_time),
			place_c_time(_place_c_time), place_l_time(_place_l_time), 
			place_t_time(_place_t_time) {}

		enum Position {
			T_1 = 1,
			T_2 = 2,
			T_3 = 3,
			T_4 = 4,
			T_5 = 5,
			T_6 = 6,
			L = 7,
			C = 8,
			B = 9
		};

		friend class Buffer;
		friend class Center;
		friend class RobotSoak;
		friend class TCM;

	private:
		class B2T2LJob : public RobotTCMJob {
			public:
				B2T2LJob(Tick _start_time, RobotTCM *_robot_tcm, TCM *_tcm, Position _tcm_pos) :
					RobotTCMJob(_start_time, _robot_tcm), tcm(_tcm), tcm_pos(_tcm_pos) {
						if(robot_tcm->checkPos(B)) {
							state = FETCH_B;
						} else {
							state = ROTATE_TO_B;
						}
					}
				void process();
			private:
				enum State {
					ROTATE_TO_B,
					FETCH_B,
					ROTATE_TO_T,
					FETCH_T,
					PLACE_T,
					ROTATE_TO_L,
					PLACE_L,
					COMPLETE
				};
				TCM *tcm;
				Position tcm_pos;
				State state;
		};

		class B2TJob : public RobotTCMJob {
			public:
				B2TJob(Tick _start_time, RobotTCM *_robot_tcm, TCM *_tcm, Position _tcm_pos) :
					RobotTCMJob(_start_time, _robot_tcm), tcm(_tcm), tcm_pos(_tcm_pos) {
						if(robot_tcm->checkPos(B)) {
							state = FETCH_B;
						} else {
							state = ROTATE_TO_B;
						}
					}
				void process();
			private:
				enum State {
					ROTATE_TO_B,
					FETCH_B,
					ROTATE_TO_T,
					PLACE_T,
					COMPLETE
				};
				TCM *tcm;
				Position tcm_pos;
				State state;
		};

		class T2LJob : public RobotTCMJob {
			public:
				T2LJob(Tick _start_time, RobotTCM *_robot_tcm, TCM *_tcm, Position _tcm_pos) :
					RobotTCMJob(_start_time, _robot_tcm), tcm(_tcm), tcm_pos(_tcm_pos) {
						if(robot_tcm->checkPos(tcm_pos)) {
							state = FETCH_T;
						} else {
							state = ROTATE_TO_T;
						}
					}
				void process();
			private:
				enum State {
					ROTATE_TO_T,
					FETCH_T,
					ROTATE_TO_L,
					PLACE_L,
					COMPLETE
				};
				TCM *tcm;
				Position tcm_pos;
				State state;
		};

		class L2CJob : public RobotTCMJob {
			public:
				L2CJob(Tick _start_time, RobotTCM *_robot_tcm) :
					RobotTCMJob(_start_time, _robot_tcm) {
						if(robot_tcm->checkPos(L)) {
							state = FETCH_L;
						} else {
							state = ROTATE_TO_L;
						}
					}
				void process();
			private:
				enum State {
					ROTATE_TO_L,
					FETCH_L,
					ROTATE_TO_C,
					PLACE_C,
					COMPLETE
				};
				State state;
		};


		class RobotTCMEvent : public Event {
			public:
				explicit RobotTCMEvent(Tick _start_time, RobotTCMJob *_job) :
					Event(_start_time), job(_job) {}
				void process();
			private:
				RobotTCMJob *job;
		};

		class InitRobotTCMJobEvent : public Event {
			public:
				explicit InitRobotTCMJobEvent(Tick _start_time, RobotTCM *_robot_soak) :
					Event(_start_time), robot_soak(_robot_soak) {}
				void process() { /*Dummy event to trigger initJob()*/ }
			private:
				RobotTCM *robot_soak;
		};

	public:
		bool busy() const {return (current_job != nullptr);}
		bool checkPos(Position _pos) const {return (_pos == pos);}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		Center *getCenter() const {return center;}
		Buffer *getBuffer() const {return buffer;}
		Loader *getLoader() const {return loader;}
		RobotSoak *getRobotSoak() const {return robot_soak;}
		RobotTCMJob *getCurrentJob() {return current_job;}
		const std::vector<TCM*> *getTCMs() const {return &tcms;}
		Tick getRotateTime() const {return rotate_time;}
		Tick getFetchBTime() const {return fetch_b_time;}
		Tick getFetchLTime() const {return fetch_l_time;}
		Tick getFetchTTime() const {return fetch_t_time;}
		Tick getPlaceCTime() const {return place_c_time;}
		Tick getPlaceLTime() const {return place_l_time;}
		Tick getPlaceTTime() const {return place_t_time;}
		Wafer *getDryWafer() const {return dry_wafer;}
		Wafer *getWetWafer() const {return wet_wafer;}

		void addTCM(TCM *tcm) {tcms.push_back(tcm);};
		void clearDev();
		void clearJob();
		void initDev();
		void initJob(); 
		void fetchBuffer();
		void fetchLoader();
		void fetchTCM(TCM *tcm);
		void placeCenter();
		void placeLoader();
		void placeTCM(TCM *tcm);
		void setPos(Position _pos) {pos = _pos;}
		void setRobotSoak(RobotSoak *_robot_soak) {robot_soak = _robot_soak;}
		void updateState(Tick elapsed_time) {};

	private:
		EventScheduler *event_scheduler;
		RobotSoak* robot_soak;
		RobotTCMJob *current_job;
		Loader *loader;
		Center *center;
		Buffer *buffer;
		std::vector<TCM*> tcms;

		Wafer *dry_wafer;
		Wafer *wet_wafer;
		Position pos;
		Tick rotate_time;
		Tick fetch_b_time;
		Tick fetch_l_time;
		Tick fetch_t_time;
		Tick place_c_time;
		Tick place_l_time;
		Tick place_t_time;

};

#endif

