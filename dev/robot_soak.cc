
#include "../common/debug.hh"
#include "robot_soak.hh"
#include "robot_tcm.hh"

#include <typeinfo>
#include <vector>

void RobotSoak::clearDev() {
	for(auto soaker_it : soakers) {
		delete soaker_it;
	}
}

void RobotSoak::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void RobotSoak::initDev() {
	buffer->setRobotSoak(this);
	center->setRobotSoak(this);
	for(auto soaker_it : soakers) {
		soaker_it->setRobotSoak(this);
	}
}

void RobotSoak::initJob() {
	Tick current_tick = event_scheduler->getCurrentTime();
	if(current_job != nullptr) {
		return;
	}
	bool deadlock = false;
	if(robot_tcm->busy()) {
		deadlock = (center->full() &&
				buffer->full() &&
				typeid(*robot_tcm->getCurrentJob()) == typeid(RobotTCM::L2CJob));
	}
	// C2S2B Job
	Soaker *soaker;
	bool isC2S2BJob = false;
	if((is_c_prefetched && !deadlock) || 
			(!is_c_prefetched &&
			 center->full() &&
			 (center->getRestTime() <= ((pos==C)?0:rotate_time)))) {
		bool soaker_avail = false;
		for(auto soaker_it : soakers) {
			if(soaker_it->ready() && 
					soaker_it->complete()){
				soaker = soaker_it;
				soaker_avail = true;
				break;
			}
		}
		if(soaker_avail) {
			for(auto tcm_it : *(robot_tcm->getTCMs())) {
				if(!tcm_it->isReserved() &&
						(tcm_it->getRestTime() <= reserved_time_for_tcm)) {
					tcm_it->setReserved();
					isC2S2BJob = true;
					break;
				}
			}
		}
	}
	if(isC2S2BJob) {
		DEBUG_PRINT("Initialize C2S2BJob(%u) of RobotSoak @ Tick %u\n",
				soaker->getID(),
				current_tick);
		current_job = new C2S2BJob(current_tick, this, soaker, static_cast<Position>(soaker->getID()));
		current_job->process();
		return;
	}

	// C2S Job
	bool isC2SJob = false;
	if(is_c_prefetched || 
			(!is_c_prefetched &&
			 center->full() &&
			 (center->getRestTime() <= ((pos==C)?0:rotate_time)))) {
		for(auto soaker_it : soakers) {
			if(!soaker_it->full() &&
					soaker_it->ready() &&
					!soaker_it->complete()) {
				isC2SJob = true;
				soaker = soaker_it;
				break;
			}
		}
	}
	if(isC2SJob) {
		DEBUG_PRINT("Initialize C2SJob(%u) of RobotSoak @ Tick %u\n",
				soaker->getID(),
				current_tick);
		current_job = new C2SJob(current_tick, this, soaker, static_cast<Position>(soaker->getID()));
		current_job->process();
		return;
	}

	// Prefetch C Job
	if(!is_c_prefetched &&
			center->full() &&
			(center->getRestTime() <= ((pos==C)?0:rotate_time))) {
		DEBUG_PRINT("Initialize PrefetchCJob of RobotSoak @ Tick %u\n",
				current_tick);
		current_job = new PrefetchCJob(current_tick, this);
		current_job->process();
		return;
	}

	//S2B Job
	bool isS2BJob = false;
	if(!is_c_prefetched && !center->full()) {
		bool soaker_avail = false;
		for(auto soaker_it : soakers) {
			if(soaker_it->ready() &&
					soaker_it->complete()) {
				soaker = soaker_it;
				soaker_avail = true;
				break;
			}
		}
		if(soaker_avail) {
			for(auto tcm_it : *(robot_tcm->getTCMs())) {
				if(!tcm_it->isReserved() && 
						(tcm_it->getRestTime() <= reserved_time_for_tcm)) {
					tcm_it->setReserved();
					isS2BJob = true;
					break;
				}
			}
		}
	}
	if(isS2BJob) {
		DEBUG_PRINT("Initialize S2BJob(%u) of RobotSoak @ Tick %u\n",
				soaker->getID(),
				current_tick);
		current_job = new S2BJob(current_tick, this, soaker, static_cast<Position>(soaker->getID()));
		current_job->process();
		return;
	}
}


void RobotSoak::fetchCenter() {
	assert(dry_wafer == nullptr);
	dry_wafer = center->getAndRemoveWafer();
}

void RobotSoak::fetchSoaker(Soaker *soaker) {
	assert(wet_wafer == nullptr);
	wet_wafer = soaker->getAndRemoveWafer();
}

void RobotSoak::placeSoaker(Soaker *soaker) {
	assert(dry_wafer != nullptr);
	soaker->addWafer(dry_wafer);
	dry_wafer = nullptr;
}

void RobotSoak::placeBuffer() {
	assert(wet_wafer != nullptr);
	buffer->addWafer(wet_wafer);
	wet_wafer = nullptr;
}

void RobotSoak::C2S2BJob::process() {
	assert(robot_soak->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_soak->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotSoakEvent *event;
	Buffer *buffer;
	switch(state) {
		case ROTATE_TO_C:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: ROTATE_TO_C @ Tick %u\n",
					soaker->getID(),
					current_tick);
			state = FETCH_C;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_C:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: FETCH_C, DRY_WID: %u @ Tick %u\n",
					soaker->getID(),
					robot_soak->getCenter()->getWafer()->getID(),
					current_tick);
			robot_soak->setPos(C);
			state = ROTATE_TO_S;
			event = new RobotSoakEvent(current_tick + robot_soak->getFetchCTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_S:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: ROTATE_TO_S @ Tick %u\n",
					soaker->getID(),
					current_tick);
			if(robot_soak->getDryWafer() == nullptr) {
				robot_soak->fetchCenter();
			}
			state = FETCH_S;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_S:
			robot_soak->setPos(soaker_pos);
			if(soaker->checkPos(Soaker::UP)) {
				DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: FETCH_S, WET_WID: %u @ Tick %u\n", 
						soaker->getID(), 
						soaker->getFirstReadyWafer()->getID(),
						current_tick);
				state = PLACE_S;
				event = new RobotSoakEvent(current_tick + robot_soak->getFetchSTime(), this);
				event_scheduler->schedule(event);
			} else {
				DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: FETCH_S (wait for soaker rising), WET_ID: %u @ Tick %u\n", 
						soaker->getID(),
						soaker->getFirstReadyWafer()->getID(),
						current_tick);
				soaker->setPendingRobotSoakJob(this);
				state = FETCH_S;
			}
			break;
		case PLACE_S:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: PLACE_S, DRY_WID: %u @ Tick %u\n", 
					soaker->getID(), 
					robot_soak->getDryWafer()->getID(),
					current_tick);
			robot_soak->fetchSoaker(soaker);
			robot_soak->getWetWafer()->setWetWaferWaitStart(current_tick);
			state = ROTATE_TO_B;
			event = new RobotSoakEvent(current_tick + robot_soak->getPlaceSTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_B:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: ROTATE_TO_B @ Tick %u\n",
					soaker->getID(),
					current_tick);
			robot_soak->placeSoaker(soaker);
			soaker->sink();
			state = PLACE_B;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_B:
			robot_soak->setPos(B);
			buffer = robot_soak->getBuffer();
			if (buffer->full()) {
				DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: PLACE_B, WET_WID: %u (wait for buffer empty), @ Tick %u\n", 
						soaker->getID(),
						robot_soak->getWetWafer()->getID(), 
						current_tick);
				buffer->setPendingRobotSoakJob(this);
				state = PLACE_B;
			} else {
				DEBUG_PRINT("C2S2BJoB(%u) of RobotSoak: PLACE_B, WET_WID: %u, @ Tick %u\n", 
						soaker->getID(),
						robot_soak->getWetWafer()->getID(), 
						current_tick);
				state = COMPLETE;
				event = new RobotSoakEvent(current_tick + robot_soak->getPlaceBTime(), this);
				event_scheduler->schedule(event);
			}
			break;
		case COMPLETE:
			DEBUG_PRINT("C2S2BJob(%u) of RobotSoak: Completed @ Tick %u\n",
					soaker->getID(),
					current_tick);
			robot_soak->placeBuffer();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for C2SJob of RobotSoak!\n", state);
			exit(EXIT_FAILURE);
	};
}

void RobotSoak::C2SJob::process() {
	assert(robot_soak->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_soak->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotSoakEvent *event;
	switch(state) {
		case ROTATE_TO_C:
			DEBUG_PRINT("C2SJob(%u) of RobotSoak: ROTATE_TO_C @ Tick %u\n",
					soaker->getID(),
					current_tick);
			state = FETCH_C;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_C:
			DEBUG_PRINT("C2SJob(%u) of RobotSoak: FETCH_C, WID: %u @ Tick %u\n",
					soaker->getID(),
					robot_soak->getCenter()->getWafer()->getID(),
					current_tick);
			robot_soak->setPos(C);
			state = ROTATE_TO_S;
			event = new RobotSoakEvent(current_tick + robot_soak->getFetchCTime(), this);
			event_scheduler->schedule(event);
			break;
		case ROTATE_TO_S:
			DEBUG_PRINT("C2SJob(%u) of RobotSoak: ROTATE_TO_S @ Tick %u\n",
					soaker->getID(),
					current_tick);
			if(robot_soak->getDryWafer() == nullptr) {
				robot_soak->fetchCenter();
			}
			state = PLACE_S;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_S:
			robot_soak->setPos(soaker_pos);
			if(soaker->checkPos(Soaker::UP)) {
				DEBUG_PRINT("C2SJob(%u) of RobotSoak: PLACE_S, WID: %u @ Tick %u\n", 
						soaker->getID(), 
						robot_soak->getDryWafer()->getID(),
						current_tick);
				state = COMPLETE;
				event = new RobotSoakEvent(current_tick + robot_soak->getFetchCTime(), this);
				event_scheduler->schedule(event);
			} else {
				DEBUG_PRINT("C2SJob(%u) of RobotSoak: PLACE_S, WID: %u (wait for soaker rising) @ Tick %u\n", 
						soaker->getID(),
						robot_soak->getDryWafer()->getID(),
						current_tick);
				soaker->setPendingRobotSoakJob(this);
				state = PLACE_S;
			}
			break;
		case COMPLETE:
			DEBUG_PRINT("C2SJob(%u) of RobotSoak: Completed @ Tick %u\n",
					soaker->getID(),
					current_tick);
			robot_soak->placeSoaker(soaker);
			soaker->sink();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for C2SJob of RobotSoak!\n", state);
			exit(EXIT_FAILURE);
	};
}

void RobotSoak::PrefetchCJob::process() {
	assert(robot_soak->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_soak->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotSoakEvent *event;
	switch(state) {
		case ROTATE_TO_C:
			state = FETCH_C;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			DEBUG_PRINT("PrefetchCJob of RobotSoak: ROTATE_TO_C @ Tick %u\n",
					current_tick);
			break;
		case FETCH_C:
			robot_soak->setPos(C);
			state = COMPLETE;
			event = new RobotSoakEvent(current_tick + robot_soak->getFetchCTime(), this);
			event_scheduler->schedule(event);
			DEBUG_PRINT("PrefetchCJob of RobotSoak: FETCH_C, WID: %u @ Tick %u\n",
					robot_soak->getCenter()->getWafer()->getID(),
					current_tick);
			break;
		case COMPLETE:
			robot_soak->fetchCenter();
			DEBUG_PRINT("PrefetchCJob of RobotSoak: Completed, WID: %u @ Tick %u\n",
					robot_soak->getDryWafer()->getID(),
					current_tick);
			robot_soak->setCPrefetched();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for PrefetchCJob of RobotTCM!\n", state);
			exit(EXIT_FAILURE);
	};
}


void RobotSoak::S2BJob::process() {
	assert(robot_soak->getCurrentJob() == this);
	EventScheduler *event_scheduler = robot_soak->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotSoakEvent *event;
	Buffer *buffer;
	switch(state) {
		case ROTATE_TO_S:
			DEBUG_PRINT("S2BJob(%u) of RobotSoak: ROTATE_TO_S @ Tick %u\n",
					soaker->getID(),
					current_tick);
			state = FETCH_S;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case FETCH_S:
			robot_soak->setPos(soaker_pos);
			if(soaker->checkPos(Soaker::UP)) {
				DEBUG_PRINT("S2BJob(%u) of RobotSoak: FETCH_S, WID: %u @ Tick %u\n", 
						soaker->getID(), 
						soaker->getFirstReadyWafer()->getID(), 
						current_tick);
				state = ROTATE_TO_B;
				event = new RobotSoakEvent(current_tick + robot_soak->getFetchCTime(), this);
				event_scheduler->schedule(event);
			} else {
				DEBUG_PRINT("S2BJob(%u) of RobotSoak: FETCH_S (wait for soaker rising), WID: %u @ Tick %u\n", 
						soaker->getID(),
						soaker->getFirstReadyWafer()->getID(), 
						current_tick);
				soaker->setPendingRobotSoakJob(this);
				state = FETCH_S;
			}
			break;
		case ROTATE_TO_B:
			DEBUG_PRINT("S2BJob(%u) of RobotSoak: ROTATE_TO_B @ Tick %u\n",
					soaker->getID(),
					current_tick);
			robot_soak->fetchSoaker(soaker);
			robot_soak->getWetWafer()->setWetWaferWaitStart(current_tick);
			soaker->sink();
			state = PLACE_B;
			event = new RobotSoakEvent(current_tick + robot_soak->getRotateTime(), this);
			event_scheduler->schedule(event);
			break;
		case PLACE_B:
			robot_soak->setPos(B);
			buffer = robot_soak->getBuffer();
			if (buffer->full()) {
				DEBUG_PRINT("S2BJob(%u) of RobotSoak: PLACE_B, WID: %u (wait for buffer empty), @ Tick %u\n", 
						soaker->getID(),
						robot_soak->getWetWafer()->getID(), 
						current_tick);
				buffer->setPendingRobotSoakJob(this);
				state = PLACE_B;
			} else {
				DEBUG_PRINT("S2BJoB(%u) of RobotSoak: PLACE_B, WID: %u, @ Tick %u\n", 
						soaker->getID(),
						robot_soak->getWetWafer()->getID(), 
						current_tick);
				state = COMPLETE;
				event = new RobotSoakEvent(current_tick + robot_soak->getPlaceBTime(), this);
				event_scheduler->schedule(event);
			}
			break;
		case COMPLETE:
			DEBUG_PRINT("S2BJob(%u) of RobotSoak: COMPLETE @ Tick %u\n",
					soaker->getID(),
					current_tick);
			robot_soak->placeBuffer();
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for S2BJob of RobotSoak!\n", state);
			exit(EXIT_FAILURE);
	}
}

void RobotSoak::RobotSoakEvent::process() {
	assert(job != nullptr);
	job->process();
	if(job->isClearJob()) {
		RobotSoak *robot_soak = job->getRobotSoak();
		robot_soak->clearJob();
	}
}

