
#ifndef _SOAKER_HH_
#define _SOAKER_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/type.hh"
#include "../common/wafer.hh"

#include <cassert>
#include <string>
#include <queue>

class RobotSoak;
class RobotSoakJob;

class Soaker : public Device{
	public:
		explicit Soaker(EventScheduler *_event_scheduler,
				std::string _name,
				unsigned _max_soaking_wafers, unsigned _id,
				Tick _process_time, Tick _rise_time,
				Tick _sink_time, Tick _interval) : Device(_name),
			event_scheduler(_event_scheduler), current_job(nullptr),
			pending_process_job(nullptr), pending_robot_soak_job(nullptr), 
			robot_soak(nullptr), id(_id),
			max_soaking_wafers(_max_soaking_wafers), pos(DOWN),
			process_time(_process_time), rise_time(_rise_time),
			sink_time(_sink_time), interval(_interval),
			rest_interval(0) {}

		enum Position {
			UP = 1,
			MIDDLE = 2,
			DOWN = 3
		};

	private:

		class SoakerEvent;

		class SoakerJob : public Job {
			public:
				SoakerJob(Tick _start_time, Soaker *_soaker) :
					Job(_start_time), clear_job(false), soaker(_soaker) {}
				bool isClearJob() {return clear_job;}
				Soaker *getSoaker() {return soaker;}
			protected:
				bool clear_job;
				Soaker *soaker;
		};

		class ProcessJob : public SoakerJob {
			public:
				ProcessJob(Tick _start_time, Soaker *_soaker) :
					SoakerJob(_start_time, _soaker), complete_event(nullptr),
					state(PROCESS) {}
				void process();
				void setInterrupt();
			private:
				enum State {
					PROCESS,
					COMPLETE
				};
				Soaker::SoakerEvent *complete_event;
				State state;
		};

		class RiseJob : public SoakerJob {
			public:
				RiseJob(Tick _start_time, Soaker *_soaker) :
					SoakerJob(_start_time, _soaker), state(RISE) {}
				void process();
			private:
				enum State {
					RISE,
					COMPLETE
				};
				State state;
		};

		class SinkJob : public SoakerJob {
			public:
				SinkJob(Tick _start_time, Soaker *_soaker) :
					SoakerJob(_start_time, _soaker), state(SINK) {}
				void process();
			private:
				enum State {
					SINK,
					COMPLETE
				};
				State state;
		};

		class SoakerEvent : public Event {
			public:
				explicit SoakerEvent(Tick _start_time, SoakerJob *_job) :
					Event(_start_time), invalid(false), job(_job) {}
				void process();
				void setInvalid() {invalid = true;}
			private:
				bool invalid;
				SoakerJob *job;
		};

		class InitRobotSoakJobEvent : public Event {
			public:
				explicit InitRobotSoakJobEvent(Tick _start_time, 
						RobotSoak *_robot_soak, 
						Soaker *_soaker) :
					Event(_start_time), robot_soak(_robot_soak),
					soaker(_soaker) {}
				void process();
			private:
				RobotSoak *robot_soak;
				Soaker *soaker;
		};

	public:
		bool checkPos(Position _pos) {return (pos == _pos);}
		bool complete () const {return (ready_wafers.size() > 0);}
		bool full() const {return ((unready_wafers.size() + ready_wafers.size()) ==
				max_soaking_wafers);}
		bool hasPendingProcessJob() const {return (pending_process_job != nullptr);}
		bool hasPendingRobotSoakJob() const {return (pending_robot_soak_job != nullptr);}
		bool ready() const {return (rest_interval == 0 && pos == DOWN);}
		bool unready_empty() const {return (unready_wafers.size() == 0);}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		RobotSoak *getRobotSoak() const {return robot_soak;}
		SoakerJob *getCurrentJob() const {return current_job;}
		Tick getProcessTime() const {return process_time;}
		Tick getRiseTime() const {return rise_time;}
		Tick getRestInterval() const {return rest_interval;}
		Tick getRestTime() const {return unready_wafers.front().first;}
		Tick getSinkTime() const {return sink_time;}
		Wafer *getAndRemoveWafer();
		Wafer *getFirstReadyWafer() const {return ready_wafers.front();}
		Wafer *getFirstUnreadyWafer() const {return unready_wafers.front().second;}
		unsigned getID() const {return id;}
		/*
			 void initWafer(Wafer *_wafer);
			 void initSoakJob();
		 */
		void addWafer(Wafer *wafer);
		void addReadyWafer();
		void clearJob();
		void continueProcessJob();
		void initProcessJob();
		void initRobotSoakJob();
		void responseRobotSoakJob();
		void rise();
		void sink();
		void setPendingRobotSoakJob(RobotSoakJob* job) {
			assert(pending_robot_soak_job == nullptr);
			pending_robot_soak_job = job;
		}
		void setRestInterval() {rest_interval = ((interval<getRestTime())?interval:getRestTime());}
		void setPos(Position _pos) {pos = _pos;}
		void setRobotSoak(RobotSoak *_robot_soak) {robot_soak = _robot_soak;}
		void updateState(Tick elapsed_time);

	private:
		EventScheduler *event_scheduler;
		SoakerJob *current_job;
		ProcessJob *pending_process_job;
		RobotSoakJob* pending_robot_soak_job;
		RobotSoak *robot_soak;

		std::deque<std::pair<Tick, Wafer*> > unready_wafers;
		std::queue<Wafer*> ready_wafers;
		unsigned id;
		unsigned max_soaking_wafers;
		Position pos;
		Tick process_time;
		Tick rise_time;
		Tick sink_time;
		Tick interval;
		Tick rest_interval;
};

#endif

