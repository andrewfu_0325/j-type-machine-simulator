
#ifndef _TCM_HH_
#define _TCM_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/type.hh"
#include "../common/wafer.hh"

#include <cassert>
#include <string>

class RobotTCM;
class RobotTCMJob;

class TCM : public Device{
	public:
		explicit TCM(EventScheduler *_event_scheduler, 
				std::string _name,
				Tick _process_time, Tick _reserved_time_for_tcm, 
				unsigned _id) : Device(_name),
			event_scheduler(_event_scheduler), current_job(nullptr),
			robot_tcm(nullptr), reserved(nullptr), wafer(nullptr), 
			process_time(_process_time), rest_time(0),
			reserved_time_for_tcm(_reserved_time_for_tcm), id(_id) {}
	private:
		class TCMJob : public Job {
			public:
				TCMJob(Tick _start_time, TCM *_tcm) :
					Job(_start_time), clear_job(false), tcm(_tcm) {}
				bool isClearJob() {return clear_job;}
				TCM *getTCM() {return tcm;}
			protected:
				bool clear_job;
				TCM *tcm;
		};

		class ProcessJob : public TCMJob {
			public:
				ProcessJob(Tick _start_time, TCM *_tcm) :
					TCMJob(_start_time, _tcm), state(PROCESS) {}
				void virtual process();
			private:
				enum State {
					PROCESS,
					COMPLETE
				};
				State state;
		};

		class TCMEvent : public Event {
			public:
				explicit TCMEvent(Tick _start_time, TCMJob *_job) :
					Event(_start_time), job(_job) {}
				void process();
			private:
				TCMJob *job;
		};

	public:
		bool complete () const {return rest_time == 0;}
		bool full() const {return (wafer != nullptr);}
		bool isReserved() const {return reserved;}
		TCMJob *getCurrentJob() const {return current_job;}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		RobotTCM *getRobotTCM() const {return robot_tcm;}
		Tick getProcessTime() const {return process_time;}
		Tick getRestTime() const {return rest_time;}
		Wafer *getAndRemoveWafer();
		Wafer *getWafer() const {return wafer;}
		unsigned getID() const {return id;}

		void addWafer(Wafer *wafer);
		void clearJob();
		void initRobotSoakJob();
		void initRobotTCMJob();
		void resetReserved() {reserved = false;};
		void setReserved() {reserved = true;};
		void setRobotTCM(RobotTCM *_robot_tcm) {robot_tcm = _robot_tcm;}
		void updateState(Tick elapsed_time);

	private:
		EventScheduler *event_scheduler;
		TCMJob *current_job;
		RobotTCM *robot_tcm;

		bool reserved;
		Wafer *wafer;
		Tick process_time;
		Tick rest_time;
		Tick reserved_time_for_tcm;
		unsigned id;
};

#endif

