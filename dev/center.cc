
#include "../common/event.hh"
#include "../common/debug.hh"
#include "center.hh"
#include "robot_tcm.hh"
#include "robot_soak.hh"

#include <cassert>

Wafer *Center::getAndRemoveWafer() {
	assert(wafer != nullptr);
	assert(rest_time == 0);
	wafer->setPickTime(event_scheduler->getCurrentTime());
	Wafer *tmp = wafer;
	wafer = nullptr;
	if(pending_robot_tcm_job != nullptr) {
		Tick current_tick = event_scheduler->getCurrentTime();
		RobotTCM::RobotTCMEvent *event= new RobotTCM::RobotTCMEvent(
				current_tick + 0, 
				pending_robot_tcm_job);
		pending_robot_tcm_job = nullptr;
		event_scheduler->schedule(event);
	}
	return tmp;
}

void Center::addWafer(Wafer *_wafer) {
	assert(wafer == nullptr);
	assert(current_job == nullptr);
	wafer = _wafer;
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	setRestTime();
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new ProcessJob(current_tick, this);
	current_job->process();
}

void Center::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void Center::initRobotTCMJob() {
	Tick init_robot_tcm_job_time = event_scheduler->getCurrentTime();
	Tick rotate_time_tcm = robot_tcm->getRotateTime();
	Tick fetch_l_time = robot_tcm->getFetchLTime();
	RobotTCM::InitRobotTCMJobEvent *init_robot_tcm_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init L2C Job */
	reserved_time = rest_time - 2 * rotate_time_tcm - fetch_l_time;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);
	reserved_time = rest_time - rotate_time_tcm - fetch_l_time;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);

	for(auto it : reserved_times) {
		init_robot_tcm_job_time += it;
		init_robot_tcm_job_event = new RobotTCM::InitRobotTCMJobEvent(
				init_robot_tcm_job_time,
				robot_tcm);
		event_scheduler->schedule(init_robot_tcm_job_event);
	}
}

void Center::initRobotSoakJob() {
	Tick init_robot_soak_job_time = event_scheduler->getCurrentTime();
	Tick rotate_time_soak = robot_soak->getRotateTime();
	RobotSoak::InitRobotSoakJobEvent *init_robot_soak_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init C2S and C2S2B Job */
	reserved_time = rest_time - rotate_time_soak;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);
	for(auto it : reserved_times) {
		init_robot_soak_job_time += it;
		init_robot_soak_job_event = new RobotSoak::InitRobotSoakJobEvent(
				init_robot_soak_job_time,
				robot_soak);
		event_scheduler->schedule(init_robot_soak_job_event);
	}
}

void Center::updateState(Tick elapsed_time) {
	if(rest_time > 0) {
		assert(wafer != nullptr);
		assert(rest_time >= elapsed_time);
		rest_time -= elapsed_time;
	}
}

void Center::ProcessJob::process() {
	assert(center->getCurrentJob() == this); 
	EventScheduler *event_scheduler = center->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	CenterEvent *center_event;
	switch (state) {
		case PROCESS:
			DEBUG_PRINT("ProcessJob of Center: PROCESS, WID: %u @ Tick %u\n", 
					center->getWafer()->getID(),
					current_tick);
			center->getWafer()->setProcessBeginTime(current_tick);
			center->initRobotTCMJob();
			center->initRobotSoakJob();
			center_event = new CenterEvent(current_tick + center->getProcessTime(), this);
			event_scheduler->schedule(center_event);
			state = COMPLETE;
			break;
		case COMPLETE:
			DEBUG_PRINT("ProcessJob of Center: COMPLETE, WID: %u @ Tick %u\n",
					center->getWafer()->getID(),
					current_tick);
			center->getWafer()->setProcessEndTime(current_tick);
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for ProcessJob of Center!\n", state);
			break;
	}
}

void Center::CenterEvent::process() {
	assert(job != nullptr);
	job->process();
	if(job->isClearJob()) {
		Center *center = job->getCenter();
		center->clearJob();
	}
}

