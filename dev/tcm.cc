
#include "../common/event.hh"
#include "../common/debug.hh"
#include "tcm.hh"
#include "robot_soak.hh"
#include "robot_tcm.hh"

#include <cassert>
#include <vector>

Wafer *TCM::getAndRemoveWafer() {
	assert(wafer != nullptr);
	assert(rest_time == 0);
	wafer->setPickTime(event_scheduler->getCurrentTime());
	Wafer *tmp = wafer;
	wafer = nullptr;
	return tmp;
}

void TCM::addWafer(Wafer *_wafer) {
	assert(wafer == nullptr);
	assert(current_job == nullptr);
	wafer = _wafer;
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	rest_time = process_time;
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new ProcessJob(current_tick, this);
	current_job->process();
}

void TCM::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void TCM::initRobotSoakJob() {
	RobotSoak *robot_soak = robot_tcm->getRobotSoak();

	Tick init_robot_soak_job_time = event_scheduler->getCurrentTime();
	RobotSoak::InitRobotSoakJobEvent *init_robot_soak_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init C2S2BJob and S2BJob */
	reserved_time = rest_time -
		reserved_time_for_tcm;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);

	for(auto it : reserved_times) {
		init_robot_soak_job_time += it;
		init_robot_soak_job_event = new RobotSoak::InitRobotSoakJobEvent(
				init_robot_soak_job_time,
				robot_soak);
		event_scheduler->schedule(init_robot_soak_job_event);
	}
}

void TCM::initRobotTCMJob() {
	Tick init_robot_tcm_job_time = event_scheduler->getCurrentTime();
	Tick rotate_time_tcm = robot_tcm->getRotateTime();
	Tick fetch_b_time = robot_tcm->getFetchBTime();
	RobotTCM::InitRobotTCMJobEvent *init_robot_tcm_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init B2T2LJob */
	reserved_time = rest_time - 2 * rotate_time_tcm - fetch_b_time;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);
	reserved_time = rest_time - rotate_time_tcm - fetch_b_time;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);
	/* Init T2LJob */
	reserved_time = rest_time - rotate_time_tcm;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);

	for(auto it : reserved_times) {
		init_robot_tcm_job_time += it;
		init_robot_tcm_job_event = new RobotTCM::InitRobotTCMJobEvent(
				init_robot_tcm_job_time,
				robot_tcm);
		event_scheduler->schedule(init_robot_tcm_job_event);
	}
}

void TCM::updateState(Tick elapsed_time) {
	if(rest_time > 0) {
		assert(wafer != nullptr);
		assert(rest_time >= elapsed_time);
		rest_time -= elapsed_time;
	}
}

void TCM::ProcessJob::process() {
	assert(tcm->getCurrentJob() == this); 
	EventScheduler *event_scheduler = tcm->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	TCMEvent *tcm_event;
	switch (state) {
		case PROCESS:
			DEBUG_PRINT("ProcessJob of TCM %u: PROCESS, WID: %u @ Tick %u\n", 
					tcm->getID(),
					tcm->getWafer()->getID(), 
					current_tick);
			tcm->getWafer()->setProcessBeginTime(current_tick);
			tcm->initRobotSoakJob();
			tcm->initRobotTCMJob();
			tcm_event = new TCMEvent(current_tick + tcm->getProcessTime(), this);
			event_scheduler->schedule(tcm_event);
			state = COMPLETE;
			break;
		case COMPLETE:
			DEBUG_PRINT("ProcessJob of TCM %u: COMPLETE, WID: %u @ Tick %u\n", 
					tcm->getID(),
					tcm->getWafer()->getID(), 
					current_tick);
			tcm->getWafer()->setProcessEndTime(current_tick);
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for ProcessJob of TCM!\n", state);
			break;
	}
}

void TCM::TCMEvent::process() {
	assert(job != nullptr);
	job->process();
	if(job->isClearJob()) {
		TCM *tcm = job->getTCM();
		tcm->clearJob();
	}
}

