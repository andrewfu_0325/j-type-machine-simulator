
#include "../common/event.hh"
#include "../common/debug.hh"
#include "soaker.hh"
#include "robot_soak.hh"

#include <cassert>
#include <typeinfo>

Wafer *Soaker::getAndRemoveWafer() {
	assert(ready_wafers.size() > 0);
	Wafer *wafer = ready_wafers.front();
	wafer->setPickTime(event_scheduler->getCurrentTime());
	ready_wafers.pop();
	return wafer;
}

void Soaker::addReadyWafer() {
	ready_wafers.push(unready_wafers.front().second);
	unready_wafers.pop_front();
}

void Soaker::addWafer(Wafer *wafer) {
	assert(pos==UP);
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	wafer->setProcessBeginTime(event_scheduler->getCurrentTime() + sink_time);
	unready_wafers.emplace_back(process_time, wafer);
}

void Soaker::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void Soaker::continueProcessJob() {
	assert(pos == DOWN);
	assert(current_job == nullptr);
	assert(pending_process_job != nullptr);
	current_job = static_cast<SoakerJob*>(pending_process_job);
	pending_process_job = nullptr;
	current_job->process();
}

void Soaker::initProcessJob() {
	assert(pos == DOWN);
	assert(current_job == nullptr);
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new ProcessJob(current_tick, this);
	current_job->process();
}

void Soaker::initRobotSoakJob() {
	Tick init_robot_soak_job_time = event_scheduler->getCurrentTime();
	RobotSoak::InitRobotSoakJobEvent *init_robot_soak_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init C2SJob, S2BJob and C2S2BJob*/
	reserved_time = rest_interval;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);

	for(auto it : reserved_times) {
		init_robot_soak_job_time += it;
		init_robot_soak_job_event = new RobotSoak::InitRobotSoakJobEvent(
				init_robot_soak_job_time,
				robot_soak);
		event_scheduler->schedule(init_robot_soak_job_event);
	}
}

void Soaker::responseRobotSoakJob() {
	Tick current_tick = event_scheduler->getCurrentTime();
	RobotSoak::RobotSoakEvent *event = new RobotSoak::RobotSoakEvent(
			current_tick + 0, 
			pending_robot_soak_job);
	event_scheduler->schedule(event);
	pending_robot_soak_job = nullptr;
}

void Soaker::rise() {
	assert(pos == DOWN);
	if(current_job != nullptr) {
		pending_process_job = dynamic_cast<Soaker::ProcessJob*>(current_job);
		assert(pending_process_job != nullptr);
		if(getRestTime() > 0) {
			pending_process_job->setInterrupt();
		}
		current_job = nullptr;
	}
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new RiseJob(current_tick, this);
	current_job->process();
}

void Soaker::sink() {
	assert(pos == UP);
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new SinkJob(current_tick, this);
	current_job->process();
}

void Soaker::updateState(Tick elapsed_time) {
	if(pos == DOWN) {
		if(unready_wafers.size() > 0) {
			if(getRestTime() > 0) {
				assert(getRestTime() >= elapsed_time);
				for(auto &it : unready_wafers) {
					it.first -= elapsed_time;
				}
			}
		}
		if(rest_interval > 0) {
			assert(rest_interval >= elapsed_time);
			rest_interval -= elapsed_time;
		}
	}
}

void Soaker::ProcessJob::process() {
	if(soaker->pending_process_job == this) {
		return;
	}
	assert(soaker->getCurrentJob() == this);
	EventScheduler *event_scheduler = soaker->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	SoakerEvent *event;
	switch (state) {
		case PROCESS:
			DEBUG_PRINT("ProcessJob of Soaker %u: PROCESS, WID: %u @ Tick %u\n", 
					soaker->getID(), 
					soaker->getFirstUnreadyWafer()->getID(),
					current_tick);
			soaker->initRobotSoakJob();
			state = COMPLETE;
			event = new SoakerEvent(current_tick + soaker->getRestTime(), this);
			event_scheduler->schedule(event);
			complete_event = event;
			break;
		case COMPLETE:
			DEBUG_PRINT("ProcessJob of Soaker %u: COMPLETE, WID: %u @ Tick %u\n", 
					soaker->getID(),
					soaker->getFirstUnreadyWafer()->getID(),
					current_tick);
			soaker->getFirstUnreadyWafer()->setProcessEndTime(current_tick);
			soaker->addReadyWafer();
			if(soaker->unready_empty()) {
				clear_job = true;
			} else {
				state = PROCESS;
				event = new SoakerEvent(current_tick + 0, this);
				event_scheduler->schedule(event);
			}
			break;
		default:
			DEBUG_PRINT("Undefined state %u for ProcessJob of Soaker!\n", state);
			exit(EXIT_FAILURE);
	};
}

void Soaker::ProcessJob::setInterrupt() {
	assert(complete_event != nullptr);
	state = PROCESS;
	complete_event->setInvalid();
	complete_event = nullptr;
}

void Soaker::RiseJob::process() {
	assert(soaker->getCurrentJob() == this);
	EventScheduler *event_scheduler = soaker->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	SoakerEvent *event;
	switch (state) {
		case RISE:
			DEBUG_PRINT("RiseJob of Soaker %u: RISE @ Tick %u\n",
					soaker->getID(),
					current_tick);
			soaker->setPos(MIDDLE);
			state = COMPLETE;
			event = new SoakerEvent(current_tick + soaker->getRiseTime(), this);
			event_scheduler->schedule(event);
			break;
		case COMPLETE:
			DEBUG_PRINT("RiseJob of Soaker %u: COMPLETE @ Tick %u\n",
					soaker->getID(),
					current_tick);
			soaker->setPos(UP);
			state = COMPLETE;
			if(soaker->hasPendingRobotSoakJob()) {
				soaker->responseRobotSoakJob();
			}
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u for RiseJob of Soaker!\n", state);
			exit(EXIT_FAILURE);
	}
}

void Soaker::SinkJob::process() {
	assert(soaker->getCurrentJob() == this);
	EventScheduler *event_scheduler = soaker->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	SoakerEvent *event;
	switch (state) {
		case SINK:
			DEBUG_PRINT("SinkJob of Soaker %u: SINK @ Tick %u\n",
					soaker->getID(),
					current_tick);
			soaker->setPos(MIDDLE);
			state = COMPLETE;
			event = new SoakerEvent(current_tick + soaker->getSinkTime(), this);
			event_scheduler->schedule(event);
			break;
		case COMPLETE:
			DEBUG_PRINT("SinkJob of Soaker %u: COMPLETE @ Tick %u\n",
					soaker->getID(),
					current_tick);
			soaker->setPos(DOWN);
			if(!soaker->unready_empty()) {
				soaker->setRestInterval();
			}
			state = COMPLETE;
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u for SinkJob of RobotSoak!\n", state);
			exit(EXIT_FAILURE);
	}
}

void Soaker::SoakerEvent::process() {
	assert(job != nullptr);
	if(!invalid) {
		job->process();
		if(job->isClearJob()) {
			Soaker *soaker = job->getSoaker();
			if(typeid(*job) == typeid(Soaker::SinkJob)) {
				soaker->clearJob();
				if(soaker->hasPendingProcessJob()) {
					soaker->continueProcessJob();
				} else if(!soaker->unready_empty()) {
					soaker->initProcessJob(); 
				}
			} else {
				soaker->clearJob();
			}
		}
	}
}

