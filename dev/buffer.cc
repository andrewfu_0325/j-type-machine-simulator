
#include "../common/event.hh"
#include "../common/debug.hh"
#include "buffer.hh"
#include "robot_tcm.hh"
#include "robot_soak.hh"

#include <cassert>
#include <vector>

Wafer *Buffer::getAndRemoveWafer() {
	assert(wafer != nullptr);
	assert(rest_time == 0);
	wafer->setPickTime(event_scheduler->getCurrentTime());
	Wafer *tmp = wafer;
	wafer = nullptr;
	if(pending_robot_soak_job != nullptr) {
		Tick current_tick = event_scheduler->getCurrentTime();
		RobotSoak::RobotSoakEvent *event= new RobotSoak::RobotSoakEvent(
				current_tick + 0, 
				pending_robot_soak_job);
		pending_robot_soak_job = nullptr;
		event_scheduler->schedule(event);
	}
	return tmp;
}

void Buffer::addWafer(Wafer *_wafer) {
	assert(wafer == nullptr);
	assert(current_job == nullptr);
	wafer = _wafer;
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	rest_time = process_time;
	Tick current_tick = event_scheduler->getCurrentTime();
	current_job = new ProcessJob(current_tick, this);
	current_job->process();
}

void Buffer::clearJob() {
	assert(current_job != nullptr);
	delete current_job;
	current_job = nullptr;
}

void Buffer::initRobotTCMJob() {
	Tick init_robot_tcm_job_time = event_scheduler->getCurrentTime();
	Tick rotate_time_tcm = robot_tcm->getRotateTime();
	RobotTCM::InitRobotTCMJobEvent *init_robot_tcm_job_event;
	std::vector<Tick> reserved_times;
	Tick reserved_time;
	/* Init B2TJob and B2T2LJob */
	reserved_time = rest_time - rotate_time_tcm;
	if(reserved_time > 0) reserved_times.push_back(reserved_time);

	for(auto it : reserved_times) {
		init_robot_tcm_job_time += it;
		init_robot_tcm_job_event = new RobotTCM::InitRobotTCMJobEvent(
				init_robot_tcm_job_time,
				robot_tcm);
		event_scheduler->schedule(init_robot_tcm_job_event);
	}
}

void Buffer::updateState(Tick elapsed_time) {
	if(rest_time > 0) {
		assert(wafer != nullptr);
		assert(rest_time >= elapsed_time);
		rest_time -= elapsed_time;
	}
}

void Buffer::ProcessJob::process() {
	assert(buffer->getCurrentJob() == this); 
	EventScheduler *event_scheduler = buffer->getEventScheduler();
	Tick current_tick = event_scheduler->getCurrentTime();
	BufferEvent *buffer_event;
	switch (state) {
		case PROCESS:
			DEBUG_PRINT("ProcessJob of Buffer: PROCESS, WID: %u @ Tick %u\n", 
					buffer->getWafer()->getID(), 
					current_tick);
			buffer->getWafer()->setProcessBeginTime(current_tick);
			buffer->initRobotTCMJob();
			buffer_event = new BufferEvent(current_tick + buffer->getProcessTime(), this);
			event_scheduler->schedule(buffer_event);
			state = COMPLETE;
			break;
		case COMPLETE:
			DEBUG_PRINT("ProcessJob of Buffer: COMPLETE, WID: %u @ Tick %u\n", 
					buffer->getWafer()->getID(),
					current_tick);
			buffer->getWafer()->setProcessEndTime(current_tick);
			clear_job = true;
			break;
		default:
			DEBUG_PRINT("Undefined state %u\n for ProcessJob of Buffer!\n", state);
			break;
	}
}

void Buffer::BufferEvent::process() {
	assert(job != nullptr);
	job->process();
	if(job->isClearJob()) {
		Buffer *buffer = job->getBuffer();
		buffer->clearJob();
	}
}

