
#include "loader.hh"

#include <cstdlib>
#include <string>

void Loader::addWafer(Wafer *wafer) {
	pending_wafers.push(wafer);
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	wafer->setProcessBeginTime(event_scheduler->getCurrentTime());
	wafer->setProcessEndTime(event_scheduler->getCurrentTime());
}

void Loader::addCompleteWafer(Wafer *wafer) {
	complete_wafers.push(wafer);
	wafer->addTimeStampForModule();
	wafer->setPlaceTime(name, event_scheduler->getCurrentTime());
	wafer->setProcessBeginTime(event_scheduler->getCurrentTime());
	wafer->setProcessEndTime(event_scheduler->getCurrentTime());
	wafer->setPickTime(event_scheduler->getCurrentTime());
}

Wafer *Loader::getAndRemoveWafer() {
	Wafer *tmp = pending_wafers.front();
	pending_wafers.pop();
	tmp->setPickTime(event_scheduler->getCurrentTime());
	return tmp;
}

void Loader::dumpResults(std::string arg_str) {
	FILE *fout = fopen("timestamp.csv", "w");
	std::string result_str, output_str;
	output_str += 
		",,,LULA,,,,,HOBIn,,,,,SoakingTank,,,,,HOBOut,,,,,Stripper,,,,,LULA,,,,WaitingTime\n";
	output_str += 
		"0,Name,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,StationName,PlaceTime,ProcessBeginTime,ProcessEndTime,PickTime,\n";

	Tick tot_time = complete_wafers.front()->getCompleteTime();
	unsigned line_number = 1;
	while(!complete_wafers.empty()) {
		Wafer *wafer = complete_wafers.front();
		complete_wafers.pop();
		Tick wet_wafer_waiting_time = 
			wafer->getWetWaferWaitEnd() - wafer->getWetWaferWaitStart();
		if(wet_wafer_waiting_time > wet_wafer_tolerence_time) {
			fprintf(stderr, "WaitingTime %u is longer than the time constraint %u\n", 
					wet_wafer_waiting_time, wet_wafer_tolerence_time);
		}
		output_str += std::to_string(line_number) + ',' +
			wafer->timeStampToString() +
			std::to_string(wet_wafer_waiting_time)+ '\n';
		line_number++;
		if(complete_wafers.empty()) {
			tot_time = wafer->getCompleteTime() - tot_time;
			double average_output_time = (double)tot_time/((double)num_wafers - 1);
			double wph = 3600/average_output_time;
			result_str += "WPH," + std::to_string(wph) + '\n' +
				"Average output time," + std::to_string(average_output_time) + '\n';
			fprintf(stdout, "WPH: %f\n", wph);
		}
		delete wafer;
	}
	fputs(result_str.c_str(), fout);
	fputs(arg_str.c_str(), fout);
	fputs(output_str.c_str(), fout);
	fclose(fout);
}
