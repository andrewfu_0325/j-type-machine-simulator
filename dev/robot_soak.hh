
#ifndef _ROBOT_SOAK_HH_
#define _ROBOT_SOAK_HH_

#include "../common/event.hh"
#include "../common/device.hh"
#include "../common/job.hh"
#include "../common/wafer.hh"
#include "buffer.hh"
#include "center.hh"
#include "soaker.hh"

#include <cassert>
#include <string>

class RobotTCM;

class RobotSoakJob : public Job {
	public:
		RobotSoakJob(Tick _start_time, RobotSoak *_robot_soak) :
			Job(_start_time), clear_job(false), robot_soak(_robot_soak) {}
		bool isClearJob() {return clear_job;}
		RobotSoak *getRobotSoak() {return robot_soak;}
	protected:
		bool clear_job;
		RobotSoak *robot_soak;
};

class RobotSoak : public Device {
	public:
		explicit RobotSoak(EventScheduler *_event_scheduler,
				std::string _name,
				Center *_center, Buffer *_buffer,
				Tick _rotate_time, Tick _fetch_c_time,
				Tick _fetch_s_time, Tick _place_b_time, 
				Tick _place_s_time, Tick _reserved_time_for_tcm) : Device(_name),
			event_scheduler(_event_scheduler), 
			robot_tcm(nullptr), current_job(nullptr),
			center(_center), buffer(_buffer),
			is_c_prefetched(false),
			dry_wafer(nullptr), wet_wafer(nullptr), pos(C),
			rotate_time(_rotate_time), fetch_c_time(_fetch_c_time),
			fetch_s_time(_fetch_s_time), place_b_time(_place_b_time),
			place_s_time(_place_s_time), reserved_time_for_tcm(_reserved_time_for_tcm) {}

		enum Position {
			S_1 = 1,
			S_2 = 2,
			S_3 = 3,
			S_4 = 4,
			S_5 = 5,
			S_6 = 6,
			C = 7,
			B = 8,
		};

		friend class Buffer;
		friend class Center;
		friend class RobotTCM;
		friend class Soaker;
		friend class TCM;

	private:

		class C2S2BJob : public RobotSoakJob {
			public:
				C2S2BJob(Tick _start_time, RobotSoak *_robot_soak,
						Soaker *_soaker, Position _soaker_pos) :
					RobotSoakJob(_start_time, _robot_soak), soaker(_soaker),
					soaker_pos(_soaker_pos) {
						if(robot_soak->getDryWafer() != nullptr) {
							/* RobotSoak must be Position C after Prefetching C */
							assert(robot_soak->checkPos(C));
							robot_soak->resetCPrefetched();
							state = ROTATE_TO_S;
						} else {
							if(robot_soak->checkPos(C)) {
								state = FETCH_C;
							} else {
								state = ROTATE_TO_C;
							}
						}
						soaker->rise();
					}
				void process();
			private:
				enum State {
					ROTATE_TO_C,
					FETCH_C,
					ROTATE_TO_S,
					FETCH_S,
					PLACE_S,
					ROTATE_TO_B,
					PLACE_B,
					COMPLETE
				};
				Soaker *soaker;
				Position soaker_pos;
				State state;
		};


		class C2SJob : public RobotSoakJob {
			public:
				C2SJob(Tick _start_time, RobotSoak *_robot_soak,
						Soaker *_soaker, Position _soaker_pos) :
					RobotSoakJob(_start_time, _robot_soak), soaker(_soaker),
					soaker_pos(_soaker_pos) {
						if(robot_soak->getDryWafer() != nullptr) {
							/* RobotSoak must be Position C after Prefetching C */
							assert(robot_soak->checkPos(C));
							robot_soak->resetCPrefetched();
							state = ROTATE_TO_S;
						} else {
							if(robot_soak->checkPos(C)) {
								state = FETCH_C;
							} else {
								state = ROTATE_TO_C;
							}
						}
						soaker->rise();
					}
				void process();
			private:
				enum State {
					ROTATE_TO_C,
					FETCH_C,
					ROTATE_TO_S,
					PLACE_S,
					COMPLETE
				};
				Soaker *soaker;
				Position soaker_pos;
				State state;
		};

		class PrefetchCJob : public RobotSoakJob {
			public:
				PrefetchCJob(Tick _start_time, RobotSoak *_robot_soak) :
					RobotSoakJob(_start_time, _robot_soak) {
						if(robot_soak->checkPos(C)) {
							state = FETCH_C;
						} else {
							state = ROTATE_TO_C;
						}
					}
				void process();
			private:
				enum State {
					ROTATE_TO_C,
					FETCH_C,
					COMPLETE
				};
				State state;
		};

		class S2BJob : public RobotSoakJob {
			public:
				S2BJob(Tick _start_time, RobotSoak *_robot_soak,
						Soaker *_soaker, Position _soaker_pos) :
					RobotSoakJob(_start_time, _robot_soak), soaker(_soaker),
					soaker_pos(_soaker_pos) {
						if(!robot_soak->checkPos(soaker_pos)) {
							state = ROTATE_TO_S;
						} else {
							state = FETCH_S;
						}
						soaker->rise();
					}
				void process();
			private:
				enum State {
					ROTATE_TO_S,
					FETCH_S,
					ROTATE_TO_B,
					PLACE_B,
					COMPLETE
				};
				Soaker *soaker;
				Position soaker_pos;
				State state;
		};
		class RobotSoakEvent : public Event {
			public:
				explicit RobotSoakEvent(Tick _start_time, RobotSoakJob *_job) :
					Event(_start_time), job(_job)  {}
				void process();
			private:
				RobotSoakJob *job;
		};

		class InitRobotSoakJobEvent : public Event {
			public:
				explicit InitRobotSoakJobEvent(Tick _start_time, RobotSoak *_robot_soak) :
					Event(_start_time), robot_soak(_robot_soak) {}
				void process() {/*Dummy event to trigger initJob()*/}
			private:
				RobotSoak *robot_soak;
		};

	public:
		bool busy() const {return (current_job != nullptr);}
		bool checkPos(Position _pos) const {return (_pos == pos);}
		bool isCPrefetched() const {return is_c_prefetched;}
		Center *getCenter() const {return center;}
		Buffer *getBuffer() const {return buffer;}
		EventScheduler *getEventScheduler() const {return event_scheduler;}
		RobotSoakJob *getCurrentJob() {return current_job;}
		const std::vector<Soaker*> *getSoakers() const {return &soakers;}
		Tick getRotateTime() const {return rotate_time;}
		Tick getFetchCTime() const {return fetch_c_time;}
		Tick getFetchSTime() const {return fetch_s_time;}
		Tick getPlaceBTime() const {return place_b_time;}
		Tick getPlaceSTime() const {return place_s_time;}
		Wafer *getDryWafer() const {return dry_wafer;}
		Wafer *getWetWafer() const {return wet_wafer;}

		void addSoaker(Soaker *soaker) {soakers.push_back(soaker);};
		void clearDev();
		void clearJob();
		void initDev();
		void initJob(); 
		void fetchCenter();
		void fetchSoaker(Soaker *soaker);
		void placeBuffer();
		void placeSoaker(Soaker *soaker);
		void resetCPrefetched() {is_c_prefetched = false;}
		void setCPrefetched() {is_c_prefetched = true;}
		void setPos(Position _pos) {pos = _pos;}
		void setRobotTCM(RobotTCM *_robot_tcm) {robot_tcm = _robot_tcm;}
		void updateState(Tick elapsed_time) {}

	private:
		EventScheduler *event_scheduler;
		RobotTCM *robot_tcm;
		RobotSoakJob *current_job;
		Center *center;
		Buffer *buffer;
		std::vector<Soaker*> soakers;

		bool is_c_prefetched;
		Wafer *dry_wafer;
		Wafer *wet_wafer;
		Position pos;
		Tick rotate_time;
		Tick fetch_c_time;
		Tick fetch_s_time;
		Tick place_b_time;
		Tick place_s_time;
		Tick reserved_time_for_tcm;

};

#endif

